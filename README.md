# aes

AES encryption utility using [PyCryptodome](https://www.pycryptodome.org)
([OpenSSL](https://www.openssl.org/) aes-256-cbc command compatible)

## Example
```
>>> import aes
>>> key = "password".encode("utf-8")
>>> data = "plaintext".encode("utf-8")
>>> enc = aes.encrypt(key, data)
>>> enc
b"Salted__\xd6\xcc\x0b\xbd;L'\xea\xca`'\x0c\x96\xe0\x03\xe9$\xf2\xb5n(B\xda\x17"
>>> dec = aes.decrypt(key, enc)
>>> dec.decode("utf-8")
'plaintext'
```
