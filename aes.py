# Copyright (C) 2023 Not Unusual Tales
# SPDX-License-Identifier: MIT

"""AES encryption utility using PyCryptodome

OpenSSL aes-256-cbc command compatible
"""

from Crypto import Random
from Crypto.Cipher import AES
from Crypto.Hash import SHA256
from Crypto.Protocol.KDF import PBKDF2
from Crypto.Util import Padding

class OpenSSL:
    MAGIC = "Salted__".encode("utf-8")
    SALT_LENGTH = 8
    KEY_LENGTH = 32
    IV_LENGTH = 16
    ITER_COUNT = 10000
    HMAC_HASH = SHA256

def encrypt(key, data):
    """Encrypt a plain data with aes-256-cbc.

    Parameters:
        key (bytes): the encryption key
        data (bytes): the plain data
    Returns:
        bytes: the encrypted data
    """

    salt = Random.get_random_bytes(OpenSSL.SALT_LENGTH)
    hash = PBKDF2(
        key, salt, OpenSSL.KEY_LENGTH + OpenSSL.IV_LENGTH,
        count = OpenSSL.ITER_COUNT,
        hmac_hash_module = OpenSSL.HMAC_HASH)

    key = hash[:OpenSSL.KEY_LENGTH]
    iv  = hash[OpenSSL.KEY_LENGTH:]

    aes = AES.new(key, AES.MODE_CBC, iv)
    enc = aes.encrypt(Padding.pad(data, AES.block_size))

    return OpenSSL.MAGIC + salt + enc

def decrypt(key, data):
    """Decrypt an decrypted data with aes-256-cbc.

    Parameters:
        key (bytes): the encryption key
        data (bytes): the encrypted data
    Returns:
        bytes: the decrypted data
    """

    salt = data[len(OpenSSL.MAGIC):len(OpenSSL.MAGIC)+OpenSSL.SALT_LENGTH]
    data = data[len(OpenSSL.MAGIC)+OpenSSL.SALT_LENGTH:]
    hash = PBKDF2(
        key, salt, OpenSSL.KEY_LENGTH + OpenSSL.IV_LENGTH,
        count = OpenSSL.ITER_COUNT,
        hmac_hash_module = OpenSSL.HMAC_HASH)

    key = hash[:OpenSSL.KEY_LENGTH]
    iv  = hash[OpenSSL.KEY_LENGTH:]

    aes = AES.new(key, AES.MODE_CBC, iv)
    dec = Padding.unpad(aes.decrypt(data), AES.block_size)

    return dec
